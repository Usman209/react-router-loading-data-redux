import { useFetcher } from "react-router-dom"
import Button from "../../utils/Button";
import { updateOrder } from "../../services/apiRestaurant";

export default function UpdateOrder ( { order } )
{
  console.log(order);
  const fetcher = useFetcher();

  return (
    <fetcher.Form method="PATCH" className="text-right">
      <Button type='primary'>Make priority</Button>
    </fetcher.Form>
  )
}

export async function action({reqest,params}) {
 
  console.log(reqest);
  const data = { priority: true };
  
  await updateOrder( params.orderId, data )
  return null;
}