import { useState } from "react"
import { useNavigate } from "react-router-dom";

export default function SearchOrder ()
{
  const [ query, setQuery ] = useState()
  const navigate = useNavigate()
  function handleSubmit ( e )
  {
    e.preventDefault();
    if ( !query ) return
    navigate( `/order/${ query }` )
    setQuery('')
  }

  return (
    <form onSubmit={handleSubmit}>

    
      <input className="rounded-full px-4 py-3 text-sm bg-yellow-100 placeholder:text-stone-900 sm:w-64 transition-all duration-300 focus:w-72" placeholder="Serach order #" value={ query } onChange={ ( e ) => setQuery( e.target.value ) } />
      </form>
  )
}
