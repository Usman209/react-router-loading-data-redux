import { Link } from "react-router-dom";

export default function Button({ children, disabled, to, onClick }) {
  const className =
    'inline-block rounded-full bg-yellow-400 px-4 py-3 font-semibold uppercase disabled:cursor-not-allowed';

  if (to) {
    return (
      <Link className={className} to={to}>
        {children}
      </Link>
    );
  }
  if ( onClick )
  {
    return (
      <button onClick={onClick} disabled={disabled} className={className}>
        {children}
      </button>
    );
  }
    return (
      <button disabled={disabled} className={className}>
        {children}
      </button>
    );
}
